# ***********************************************
# ***        Loco363 - Parts - Keypad         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Keypad(generic.bases.LabelPart):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			134,
			84,
			center=True,
			properties={
				'fill': self.COLOR_UNI,
				'stroke': self.color,
				'stroke-width': 3
			}
		))
		
		# keys area
		self.add(gen_draw.shapes.Rectangle(
			c,
			102,
			62,
			center=True,
			properties={
				'fill': 'white'
			}
		))
		
		# keys
		keys = gen_draw.Drawing(c)
		for row in range(3):
			for col in range(5):
				keys.add(gen_draw.shapes.Rectangle(
					C(c, (-50 + col*20 + 1, -30 + row*20 + 1)),
					18,
					18,
					roundness=2,
					properties={
						'fill': self.COLOR_UNI
					}
				))
		self.add(keys)
	# constructor
# class Keypad


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Keypad(name='KP'), True))
