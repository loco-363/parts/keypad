# ***********************************************
# ***        Loco363 - Parts - Keypad         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Keypad(generic.bases.View):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			134 - 0.8,# 134mm outer width
			84 - 0.8,# 84mm outer height
			center=True,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': '0.8mm'
			}
		))
		
		# keys area
		self.add(gen_draw.shapes.Rectangle(
			c,
			102 + 0.6,# 102mm inner width
			62 + 0.6,# 62mm inner height
			center=True,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': '0.6mm'
			}
		))
		
		# keys
		keys = gen_draw.Drawing(c)
		self.add(keys)
		
		keys.add(self._createKey(C(c, (-40,  20)), '+'))
		keys.add(self._createKey(C(c, (-40,   0)), '-'))
		keys.add(self._createKey(C(c, (-40, -20)), '0'))
		
		for row in range(3):
			for col in range(4):
				keys.add(self._createKey(C(c, (-20 + col*20, -20 + row*20)), str(10 + row*40 + col*10)))
	# constructor
	
	def _createKey(self, c, label = None):
		''' Creates drawing for single key with label
		
		@param c - center Coords
		@param label - optional key label
		@return key drawing
		'''
		
		key = gen_draw.Drawing(c)
		
		# outline
		key.add(gen_draw.shapes.Rectangle(
			c,
			18 - 0.4,# 18mm outer width
			18 - 0.4,# 18mm outer height
			center=True,
			roundness=2,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-linejoin': 'round',
				'stroke-width': '0.4mm'
			}
		))
		
		# key top
		key.add(gen_draw.shapes.Rectangle(
			c,
			15,
			15,
			center=True,
			roundness=1,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-linejoin': 'round',
				'stroke-width': 0.5
			}
		))
		
		# optional label
		if label != None and label != '':
			key.add(gen_draw.shapes.Text(
				C(c, (0, -2)),
				label,
				properties={
					'fill': 'black',
					'style': 'font-size:16pt; font-weight:bold;',
					'text-anchor': 'middle'
				}
			))
		
		return key
	# _createKey
# class Keypad


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Keypad(), True))
