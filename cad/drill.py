# ***********************************************
# ***        Loco363 - Parts - Keypad         ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Keypad(generic.bases.Drill):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			134,
			84,
			center=True,
			properties={
				'fill': self.COLOR_UNI
			}
		))
		
		# keys area
		self.add(gen_draw.shapes.Rectangle(
			c,
			102,
			62,
			center=True,
			properties={
				'fill': 'white'
			}
		))
		
		# coords
		self.add(generic.CoordsAuto(c, pos=(0, -5, 'middle')))
	# constructor
# class Keypad


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Keypad(name='KP'), True))
